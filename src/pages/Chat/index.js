import React, { useEffect, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Form from 'react-bootstrap/Form';
import { map } from 'lodash';
import moment from 'moment';
import Button from 'react-bootstrap/Button';

import avatar from "../../assets/img/PngItem_1985222.png";
import MenuLeft from '../../components/MenuLeft';
import Input  from '../../components/InputChat';
import Message from '../../components/Message';
import { realtimeChatWrite, connectDbChat } from '../../utils/commons'
import soundMessage from '../../assets/whatsapp-campana.mp3'
import Modals from '../../components/Modal'



const Chat = (props) => {
    const { userName } = props;
    const [message, setMessage] = useState([]);
    const [modalShow, setModalShow] = useState(false);
    
    useEffect(() => {
        (async () => {
            const nameDb = "general";
            await connectDbChat(nameDb, setMessage);
        })()
    }, [])

    const soundMp3 = () => {
        var sonido = document.createElement("iframe");
        sonido.setAttribute("src", soundMessage);
        document.body.appendChild(sonido);
    }

    const sendMessage = message => {
        soundMp3()
        const nameDb = "general"
        const time = moment().format("hh:mm a")
        const userName = "Paciente";
        realtimeChatWrite(userName, message.textChat, time, nameDb)
    }

    const closeChat = () => {
        setModalShow(true)
    }

    return(
        <Row>
            <Col xs={6} md={2}>
                <header className="col-header">
                    <h4>Online</h4>
                </header>
                <section className="col-section-items">
                    <MenuLeft />
                </section>
            </Col>
            <Col xs={12} md={8}>
                <Row>
                    <Col  xs={6} md={2} className="header-menu">
                        <Col sm={3}>
                            <Image src={avatar} roundedCircle className="img-avatar-chat"/>
                        </Col>
                        <Col sm={8} className="item-text-chat"><p><strong>Doc. Juan Pablo</strong></p></Col>
                    </Col>
                    <Col xs={12} md={8} className="close-btn-chat">
                        <Button
                            onClick={() => closeChat()}
                            variant="danger">Cerrar chat
                        </Button>
                    </Col>
                </Row>
                <Row className="col-container-chats">
                    <Row>
                    {map(message, (message, index) => {
                        return(
                            <Message
                                key={index}
                                message={message}
                                name={userName}
                            />
                        )
                    })}
                    </Row>
                </Row>
                <Row className="bg-input-text">
                    <Col>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                            <Input sendMessage={sendMessage} />
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Col>

            <Modals
                show={modalShow}
                textDescription="Al salir del chat podras acceder al historico de tu conversación dentro de tu perfil "
                textBody="Deseas salir del Chat?"
                success={{visible: true, link: '/profile'}}

                onHide={() => setModalShow(false)}
            />
        </Row>
	)
}

export default Chat;