import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import { singIn, defaultFormValue } from '../../../utils/commons';


const UserFormClinic = () => {

    /* weight,
        fever,
        height,
        allergy,
        last_consultation,
        observations,
        name,
        year,
        sexo,
        city,
        idUser, */
    
    const [formData, setFormData] = useState(defaultFormValue());

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.target.value })
    }

    return (
        <Form>
            <Form.Group controlId="formGroupWeight" className="space-group-m1">
                <Form.Label>Peso</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Peso"
                    onChange={e => onChange(e, "peso")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupFever" className="space-group-m1">
                <Form.Label>Fiebre</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Fiebre"
                    onChange={e => onChange(e, "fever")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupHeight" className="space-group-m1">
                <Form.Label>Altura</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Altura"
                    onChange={e => onChange(e, "height")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupAllergy" className="space-group-m1">
                <Form.Label>Alergia</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Alergia"
                    onChange={e => onChange(e, "allergy")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupLastConsultation" className="space-group-m1">
                <Form.Label>Última consulta</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Última consulta"
                    onChange={e => onChange(e, "last_consultation")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupLastobservations" className="space-group-m1">
                <Form.Label>Observaciones</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Observaciones"
                    onChange={e => onChange(e, "observations")}
                />
            </Form.Group>
            
        </Form>
    )
}

export default UserFormClinic