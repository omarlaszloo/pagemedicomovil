import React, {
  useState,
  useEffect,
} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt, faThLarge } from '@fortawesome/free-solid-svg-icons';
import Menu from '../../components/Menu';
import Modals from '../../components/Modal';
import ModalForm from '../../components/ModalForm';
import UserFormClinic from './UserForm/UserFormClinic';
import AddDoctor from './SettingForm/AddDoctor';
import Formula from './SettingForm/Formula';
import UserProfile from './UserForm/UserProfile';
import ProfileHeader from '../../components/ProfileHeader';
import ClinicalData from '../../components/MyClinicalData';
import MyConsults from '../../components/MyConsults';
import MyProfile from '../../components/MyProfile';
import InitChat from '../../components/InitChat';
import Payment from '../../components/Payment';
import Winnings from '../../components/Winnings';
import WinningsXMedic from '../../components/WinningsXMedic';
import { LIST_MENU } from '../../utils/data/menuSetting';
import { savePatient, currentUser, getDbFirestore } from '../../utils/commons';
import ganancias from '../../utils/data/gananciasXMedico/index.json';

import './index.css';

const Profile = () => {
  const [menu, setMenu] = useState('Mis Datos Clinicos');
  const [modalShow, setModalShow] = useState(false);
  const [modalShowForm, setModalShowForm] = useState(false);
  const [currentPath, setCurrentPath] = useState();
  const [userInfo, setUserInfo] = useState();

  const [clinicalData, setClinicalData] = useState([]);
  const [userProfileData, setUserProfileData] = useState([]);
  const [myActivity, setMyActivity] = useState([]);

  // formularios
  const [formData, setFormData] = useState();

  useEffect(() => {
    typeItemMenu('Agregar Médico')
    setMenu('Agregar Médico')
    setCurrentPath(document.location.pathname);
    const user = currentUser().then(resp => console.log('XXXXX', resp));
    console.log('user >>>>', user)
    setUserInfo(user)

    getDbFirestore({ name: 'doctor' }).then(resp => setClinicalData(resp));
  }, []);

  const name = currentPath === '/setting' ? 'Juanito' : 'Paciente'

  const section = (params) => {
    console.log('params', params)
    setMenu(params)
    typeItemMenu(params)
  }

  const showModalForm = () => {
    console.log('currentPath', currentPath)
    setModalShowForm(true)
  }

  const typeItemMenu = type => {
    switch (type) {
      case 'Mis Datos Clinicos':
        return (<ClinicalData
          section='Mis Datos Clinicos'
          dataClinic={clinicalData}
          currentPath={currentPath}
          showModalForm={showModalForm}
        />)
      case 'Agregar Médico':
        return (<ClinicalData
          section='Agregar Médico'
          dataClinic={clinicalData}
          currentPath={currentPath}
          showModalForm={showModalForm}
        />)
      case 'Mis Consultas':
        return (<MyConsults
          section='Mis consultas'
          dataClinic={myActivity} />
        )
      case 'Mi Perfil':
        return (<MyProfile
          section='Mi Perfil'
          dataClinic={userProfileData}
          showModalForm={showModalForm}
        />)
      case 'On/Off Pagos':
        return (<Payment
          section='Pagos'
        />)
      case 'Ganancias/Comisiones':
        return (<Winnings
          section='Ganancias/Comisiones'
          dataClinic={ganancias}
          showModalForm={showModalForm}
        />)
      case 'Ganancias x Médico':
        return (<WinningsXMedic
          section='Ganancias x Médico'
          dataClinic={ganancias}
          currentPath={currentPath}
          showModalForm={showModalForm}
        />)
      case 'Chat':
        return (<InitChat
          section="chat"
          userName={name}
          currentPath={currentPath}
        />)
      default:
        break
    }
  }

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.target.value })
  }

  const actionSaveForm = () => {
    savePatient(formData, { name: 'doctor' }).then(resp => {
      if (resp === 201) {
        setModalShowForm(false)
        getDbFirestore({ name: 'doctor' }).then(resp => setClinicalData(resp));
      }

      if (resp === 500) {
        console.log('error', resp)
      }
    })
  }
  const currentDataMenu = currentPath === '/setting' ? LIST_MENU[0]['admin'] : LIST_MENU[1]['user']
  console.log('currentDataMenu**', currentDataMenu)

  // solo carga formularios a la Modal
  const userForm = type => {
    switch (type) {
      case 'Mis Datos Clinicos':
        return <UserFormClinic />
      case 'Mi Perfil':
        return <UserProfile />
      default:
        break
    }
  }

  const userFormSetting = (type) => {
    switch (type) {
      case 'Agregar Médico':
        return <AddDoctor onChange={onChange} />
      case 'Mi Perfil':
        return <UserProfile />
      case 'Ganancias/Comisiones':
        return <Formula />
      default:
        break
    }
  }

  return (
    <React.Fragment>
      <div className="container">
        <div className="content-column-left">
          <div className="content-avatar">
            <ProfileHeader
              name={name}
              currentPath={currentPath}
            />
          </div>
          <div className="content-menu">
            <Menu
              name={currentPath === '/setting' ? "admin" : "user"}
              items={currentDataMenu}
              actionItem={section}
            />
          </div>
        </div>
        <div className="content-column-right">
          <div className="content-header">
            <div className="colum-left">
              <FontAwesomeIcon
                icon={faThLarge}
                size="2x"
                className="icon"
              />
              <p>Dashboard</p>
            </div>
            <div className="column-right">
              <FontAwesomeIcon
                icon={faSignInAlt}
                size="2x"
                className="icon"
              />
              <p>Cerrar sesion</p>
            </div>
          </div>
          <div className="content-table">
            <div className="bg-card">
              {typeItemMenu(menu)}
            </div>
          </div>
        </div>
      </div>

      <Modals
        show={modalShow}
        onHide={() => setModalShow(false)}
        success={{ visible: true }}
      />
      <ModalForm
        title="Agregar datos clinicos"
        show={modalShowForm}
        actionSaveForm={actionSaveForm}
        onHide={() => setModalShowForm(false)}
      >
        {
          document?.location?.pathname === '/setting' ?
            userFormSetting(menu) : userForm(menu)
        }
      </ModalForm>
      <Modals
        title="Configuracion de comisiones"
        success={{ visible: true }}
      />

    </React.Fragment>
  )
}

export default Profile