import React from 'react'
import Form from 'react-bootstrap/Form'

const AddDoctor = ({ onChange }) => {

  return (
    <Form>
      <Form.Group controlId="formGroupName" className="space-group-m1">
        <Form.Label>Nombre</Form.Label>
        <Form.Control
          type="text"
          placeholder="Nombre"
          onChange={e => onChange(e, "name")}
        />
      </Form.Group>
      <Form.Group controlId="formGroupYear" className="space-group-m1">
        <Form.Label>Edad</Form.Label>
        <Form.Control
          type="text"
          placeholder="Edad"
          onChange={e => onChange(e, "year")}
        />
      </Form.Group>
      <Form.Group controlId="formGroupSexo" className="space-group-m1">
        <Form.Label>Sexo</Form.Label>
        <Form.Control
          type="text"
          placeholder="Sexo"
          onChange={e => onChange(e, "sexo")}
        />
      </Form.Group>
      <Form.Group controlId="formGroupCity" className="space-group-m1">
        <Form.Label>Ciudad</Form.Label>
        <Form.Control
          type="text"
          placeholder="Ciudad"
          onChange={e => onChange(e, "city")}
        />
      </Form.Group>
      <Form.Group controlId="formGroupCity" className="space-group-m1">
        <Form.Label>Correo</Form.Label>
        <Form.Control
          type="text"
          placeholder="Correo"
          onChange={e => onChange(e, "email")}
        />
      </Form.Group>
    </Form>
  )
}

export default AddDoctor