import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import { defaultFormValue } from '../../../utils/commons';


const Formula = () => {

    const [formData, setFormData] = useState(defaultFormValue());

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.target.value })
    }

    return(
        <Form>
            <Form.Group controlId="formGroupName" className="space-group-m1">
                <Form.Label>Porcentaje que se aplicará a los pagos x médico</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="%"
                    onChange={e => onChange(e, "name")}
                />
            </Form.Group>
        </Form>
    )
}

export default Formula