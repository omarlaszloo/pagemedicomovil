import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from 'react-bootstrap';

import Footer from '../../components/footer';
import Submit from '../../components/Button';
import Modals from '../../components/Modal';
import LoginComponent from '../../components/LoginComponent';
import RegisterComponent from '../../components/RegisterComponent';
import Loading from '../../components/Loading';
import {
  singIn,
  defaultFormValue,
  createUser,
} from '../../utils/commons';

const Login = ({ history }) => {
  const [actionTab, setActionTab] = useState('tabLogin');
  const [formData, setFormData] = useState(defaultFormValue());
  const [loading, setLoading] = useState(false);
  const [modalShow, setModalShow] = useState(false);
  const currentPath = document.location.pathname;

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.target.value });
  };

  const onChangeTab = (nameTab) => {
    setActionTab(nameTab);
  };

  const logIn = () => {
    setLoading(true);
    const { email, password } = formData;
    singIn(email, password).then(resp => {
      if (resp === 200) {
        history.push(currentPath === '/admin' ? '/setting' : '/profile');
        setLoading(false);
      }

      if (resp === 500) {
        setModalShow(true);
        setLoading(false);
      }
    })
  };

  const registerUser = () => {
    setLoading(true);
    createUser(formData.email, formData.password).then(resp => {
      if (resp === 200) {
        history.push(currentPath === '/admin' ? '/setting' : '/profile');
        setLoading(false);
      }
    });
  }

  return (
    <React.Fragment>
      <Loading showLoading={loading} />
      <Container fluid>
        <Row>
          <Col className="bg-left-login">
            <Col className="layer-login"></Col>
            <Col className="p1-color row-online-login">
              <p className="h1">
                <strong> Médico. En linea.</strong><br />
                <strong> {currentPath === '/admin' ? 'ADMINISTRADOR ' : 'Cuidamos de ti mientras estas en casa'}</strong>
              </p>
            </Col>
          </Col>
          <Col className="bg-col-right">
            <div className="col-right">
              {actionTab === 'tabLogin' && (
                <React.Fragment>
                  <LoginComponent
                    onChange={onChange}
                    logIn={logIn}
                  />
                  <strong>¿No tienes cuenta ?</strong>
                  <br />
                  <Submit
                    variant="outline-primary"
                    oAction={() => onChangeTab('tabRegister')}
                    text="Registrate"
                  />
                </React.Fragment>
              )}
              {actionTab === 'tabRegister' && (
                <React.Fragment>
                  <RegisterComponent
                    onChange={onChange}
                    registerUser={() => registerUser()}
                  />
                  <Submit
                    variant="outline-primary"
                    oAction={() => onChangeTab('tabLogin')}
                    text="Regresar"
                  />
                </React.Fragment>
              )}
            </div>
          </Col>
        </Row>
      </Container>
      <Row>
        <Footer />
      </Row>

      <Modals
        show={modalShow}
        textDescription="Paso algo inesperado, posiblemente tu contraseña o correo que ingresaste no es el correcto"
        textBody="Oh oh!! No se puede ingresar a tu cuenta"
        onHide={() => setModalShow(false)}
        success={{ visible: true }}
      />
    </React.Fragment>
  )
}

export default Login;