import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from 'react-bootstrap';
import Footer from '../../components/footer';
import logo from '../../assets/img/icon.png';
import { Free, Payment as Payments } from './components';
import './index.css';

const Payment = () => {
  const flow = 'payment';

  return (
    <>
      <Container fluid>
        <Row>
          <Col className="bg-left">
            <Col className="layer-bg"></Col>
            <Col className="p1-color row-online-login">
              {flow === 'free' ? (
                <p className="h2 label-title-left">
                  <p>PRUEBA GRATIS<br />
                    AGENDANDO UNA CITA</p>
                  <p className='title'>Ahora tener una consulta médica <br />de <strong>URGENCIA</strong> sin salir de casa nunca fue tan facil. <br /><br />
                  </p>
                </p>
              ) : (
                <p className="h2 label-title-left">
                  <p>CUIDAMOS DE TI</p>
                  <p>MIENTRAS ESTÁS EN CASA</p>
                </p>
              )}
            </Col>
          </Col>
          <Col className="bg-col-right">
            <div className="col-right">
              <div className="payment-logo">
                <img
                  className="logo-mobile"
                  src={logo}
                  alt="medico en movil"
                />
                <p className="label-title">Médico. En linea</p>
              </div>

              {flow === 'free' ? (
                <Free />
              ) : (
                <Payments />
              )}
            </div>
          </Col>
        </Row>
      </Container >
      <Footer />
    </>
  )
}

export default Payment;