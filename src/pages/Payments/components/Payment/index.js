import React from 'react';
import Button from '../../../../components/atoms/Button';
import '../../index.css';


const Payment = () => {

  const handlePayment = async () => {
    try {
      const resp = await fetch('http://localhost:4242/create-checkout-session', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const { response } = await resp.json();
      window.location.href = response;
    } catch (errorPayment) {
      console.log(':: errorPayment', errorPayment)
    }
  }

  const extraStyles = {
    backgroundColor: '#1A76BB',
    width: '50%',
    padding: '2.5%',
  }

  return (
    <>
      <div>
        <p>Ahora tener una consulta médica de <strong>URGENCIA</strong> sin salir de casa nunca fue tan facil. <br />
          Ahorrar tiempo y filas con tan solo un click.
        </p>
        <br />
        <p>
          En médico movil cuida de ti y de tu información.
        </p>
        <p>
          * Costo por consulta  <strong>$150</strong>
        </p>
      </div>
      <div className='content-button-payment'>
        <Button
          handleOnChange={handlePayment}
          extraStyles={extraStyles}
          label="AGENDAR CONSULTAR"
        />
      </div>
    </>
  )
}

export default Payment;