import React from 'react';
import { InlineWidget } from 'react-calendly';

const Free = () => {
  return (
    <div className='calendar'>
      <div className="App">
        <InlineWidget url="https://calendly.com/omarlaszloo" />
      </div>
    </div>
  )
}

export default Free;