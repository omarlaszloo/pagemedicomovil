import React, { useState, useEffect } from 'react';
import { isNil } from 'ramda';
import Button from 'react-bootstrap/Button';
import Modals from '../../../components/Modal';
import ModalForm from '../../../components/ModalForm';
import AddDoctor from '../SettingForm/AddDoctor';
import { savePatient, currentUser, getDbFirestore } from '../../../utils/commons';
import DataTable from '../../../components/DataTable';
import dataTableAdmin from '../../../constants/dataTableAdmin.json';

const DoctorRegister = () => {
  const [listDoctors, setListDoctors] = useState([]);
  const [modalShow, setModalShow] = useState(false);
  const [formData, setFormData] = useState();
  const [modalShowForm, setModalShowForm] = useState(false);
  const [clinicalData, setClinicalData] = useState([]);

  const configTable = {
    isEditAvailable: true,
    isDeleteAvailable: true,
    headers: dataTableAdmin,
    items: [],
  }

  const showModalForm = () => {
    setModalShowForm(true)
  }

  useEffect(() => {
    const user = currentUser().then(resp => console.log('XXXXX', resp));
    console.log('user >>>>', user)

    getDbFirestore({ name: 'doctor' }).then(resp => setListDoctors(resp));
  }, []);

  const actionSaveForm = () => {
    savePatient(formData, { name: 'doctor' }).then(resp => {
      if (resp === 201) {
        setModalShowForm(false)
        getDbFirestore({ name: 'doctor' }).then(resp => setClinicalData(resp));
      }

      if (resp === 500) {
        console.log('error', resp)
      }
    })
  }

  const listData = isNil(listDoctors) ? [] : listDoctors;

  const handleEdit = () => {
    console.log('edit')
  }

  const handleDelete = () => {
    console.log('handleDelete')
  }

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.target.value })
  }

  const notInfoUser = () => (<p>Aún no tienes médicos agregados...</p>)

  return (
    <>
      <div className='header-intrucctions'>
        <div className='intrucctions'>
          <p>Instrucciones: <br />
            1.- Para agregar un nuevo integrante al <strong>STAFF</strong><br />
            2.- Dar click en el botón Agreagar médico
          </p>
        </div>
        <div className='action-btn'>
          <Button
            onClick={() => showModalForm()}
            className="add-doctor"
          >
            Agregar médico
          </Button>
        </div>
      </div>
      {!!listData.length ? (
        <DataTable
          listData={listData}
          configTable={configTable}
          handleEdit={handleEdit}
          handleDelete={handleDelete}
        />
      ) : notInfoUser()
      }

      <Modals
        show={modalShow}
        onHide={() => setModalShow(false)}
        success={{ visible: true }}
      />
      <ModalForm
        title="Agregar médico"
        show={modalShowForm}
        actionSaveForm={actionSaveForm}
        onHide={() => setModalShowForm(false)}
      >
        <AddDoctor onChange={onChange} />
      </ModalForm>
      <Modals
        title="Configuracion de comisiones"
        success={{ visible: true }}
      />
    </>
  )
}

export default DoctorRegister;