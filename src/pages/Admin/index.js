import React, {
  useState,
} from 'react';
import { head } from 'ramda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt, faThLarge } from '@fortawesome/free-solid-svg-icons';
import Menu from '../../components/Menu';
import ProfileHeader from '../../components/ProfileHeader';
import InitChat from '../../components/InitChat';
import DoctorRegister from './DoctorRegister';
import { LIST_MENU } from '../../utils/data/menuSetting';

import './index.css';

const Admin = () => {
  const [menu, setMenu] = useState('Médico');
  const [currentPath] = useState('/setting');

  const name = 'Juanito';

  const section = (params) => {
    setMenu(params)
  }

  const currentDataMenu = head(LIST_MENU);

  const renderTabMenu = {
    'Médico': <DoctorRegister />,
    'Chat': <InitChat currentPath="setting" />
  };

  return (
    <>
      <div className="container">
        <div className="content-column-left">
          <div className="content-avatar">
            <ProfileHeader
              name={name}
              currentPath={currentPath}
            />
          </div>
          <div className="content-menu">
            <Menu
              name="admin"
              items={currentDataMenu?.admin}
              actionItem={section}
            />
          </div>
        </div>
        <div className="content-column-right">
          <div className="content-header">
            <div className="colum-left">
              <FontAwesomeIcon
                icon={faThLarge}
                size="2x"
                className="icon"
              />
              <p>Dashboard</p>
            </div>
            <div className="column-right">
              <FontAwesomeIcon
                icon={faSignInAlt}
                size="2x"
                className="icon"
              />
              <p>Cerrar sesion</p>
            </div>
          </div>
          <div className="content-table">
            <div className="bg-card">
              {renderTabMenu[menu || 'Médico']}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Admin