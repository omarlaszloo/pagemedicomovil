import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import { singIn, defaultFormValue } from '../../../utils/commons';


const UserProfile = () => {

    const [formData, setFormData] = useState(defaultFormValue());

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.target.value })
    }

    return (
        <Form>
            <Form.Group controlId="formGroupName" className="space-group-m1">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Nombre"
                    onChange={e => onChange(e, "name")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupYear" className="space-group-m1">
                <Form.Label>Edad</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Edad"
                    onChange={e => onChange(e, "year")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupSexo" className="space-group-m1">
                <Form.Label>Sexo</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Sexo"
                    onChange={e => onChange(e, "sexo")}
                />
            </Form.Group>
            <Form.Group controlId="formGroupCity" className="space-group-m1">
                <Form.Label>Ciudad</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Ciudad"
                    onChange={e => onChange(e, "city")}
                />
            </Form.Group>
        </Form>
    )
}

export default UserProfile