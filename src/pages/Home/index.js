import React from 'react';

import Navs from './navs/index';
import Footer from '../../components/footer';
import Consultation from './sectionsConsultation';
import SectionServices from './SectionServices';
import Contactos from './Contactanos';

import '../../App.css';

const Home = () => (
  <div className="App">
    <Navs />
    <SectionServices />
    <Consultation />
    <Contactos />
    <Footer />
  </div>
)

export default Home;