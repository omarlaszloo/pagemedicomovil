import React from 'react';
import Card from 'react-bootstrap/Card';

import food from '../../../assets/img/importancia-comer-saludable.png'
import sintomas from '../../../assets/img/1585919888_935236_1585919974_noticia_normal.jpg'
import gb from '../../../assets/img/gb.jpg'

import './index.css';

const items = [
  {
    img: food,
    description: 'En una persona de 170mts con 80 kilogramos en promedio quema 2000kkal cada 24 hrs eso quiere decir que necesitamos darle 25 kilocalorías por kilogramo de peso para mantenerlo en un peso ideal.'
  },
  {
    img: gb,
    description: 'El sobrepeso y la obesidad son prevenibles llevando una dieta sana y balanceada. La circunferencia de cintura es considerada otro indicador para detectar posibles riesgos de salud relacionados con la acumulación de grasa...'
  },
  {
    img: sintomas,
    description: 'Entre las bacterias de importancia clínica que con mayor frecuencia causan infecciones respiratorias destacan, en los casos de infecciones respiratorias altas, el Streptococcus pyogenes y, en los de infecciones respiratorias altas y bajas, el Streptococcus pneumoniae y el Haemphilus influenza y sincitial respiratoria.'
  }

]

const SectionServices = () => (
  <div className="content" id="servicios">
    <div className="content-card">
      {items.map((item) => {
        return (
          <div className='item-card'>
            <div>
              <img src={item.img} className='img-card' />
            </div>
            <div className='card-description'>
              <p>
                {item.description}
              </p>
            </div>
          </div>
        )
      })}
    </div>
  </div>
)

export default SectionServices;