import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

import Submit from '../../../components/Button';
import { distpatchContacts, defaultFormContacs } from '../../../utils/commons';


const Contactanos = () => {

  const [formData, setFormData] = useState(defaultFormContacs());
  const title = 'Dejanos tus comentarios \n para nosotros es muy importante atenderte';

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.target.value });
  }

  const senEmail = () => {
    const uri = 'send-contacts';
    distpatchContacts(uri, formData);
  }

  return (
    <Container fluid id="contactanos">
      <Row>
        <Col>
          <Form className="form">
            <h4 className="h4-form">{title}</h4>
            <Form.Group controlId="exampleForm.ControlInput1" className="input-form">
              <Form.Label>NOMBRE:</Form.Label>
              <Form.Control
                type="text"
                placeholder="nombre"
                onChange={e => onChange(e, "name")}
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1" className="input-form">
              <Form.Label>CORREO:</Form.Label>
              <Form.Control
                type="email"
                placeholder="name@example.com"
                onChange={e => onChange(e, "email")}
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1" className="input-form">
              <Form.Label>ESCRIBENOS TUS DUDAS:</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                onChange={e => onChange(e, "message")}
              />
            </Form.Group>
            <div className="input-form btn-send-email">
              <Submit
                variant="primary"
                oAction={() => senEmail()}
                className="btn-large"
                text="Enviar"
              />
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  )
}

export default Contactanos;