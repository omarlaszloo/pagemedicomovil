import React, { useEffect, useState } from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import { useHistory } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Button from '../../../components/atoms/Button';
import Navbar from 'react-bootstrap/Navbar';
import NAV from '../../../constants/menu.json';
import logo from '../../../assets/img/icon.png';
import googlePlay from '../../../assets/img/disponible-en-google-play-badge.svg';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const Navs = () => {
  const [scrolling, setScrolling] = useState(false);
  const [scrollTop] = useState(0);
  const navigate = useHistory();

  const handleOnChange = () => {
    navigate.push('/payment');
  };

  useEffect(() => {
    const onScroll = e => {
      setScrolling(e.target.documentElement.scrollTop === 0 ? false : true);
    }

    window.addEventListener('scroll', onScroll);

    return () => window.removeEventListener('scroll', onScroll);

  }, [scrollTop]);

  const itemsNav = (items, typeMenu = 'desktop') => (
    <> {
      typeMenu === 'desktop' && (
        <li>
          <li><img className="logo" src={logo} alt="medico en movil" /></li>
          {items.map((item) => (
            <a className="title-nav" href={`#${item?.nav}`}>
              {item.nav}
            </a>
          ))}
        </li>
      )
    }
      {typeMenu === 'movil' && (
        items.map((item) => (
          <Nav className="mr-auto">
            <Nav.Link href="#home">{item?.nav}</Nav.Link>
          </Nav>
        ))
      )}
    </>
  )

  return (
    <>
      <Col className="d-none container-layer-nav d-md-block">
        <ul>{itemsNav(NAV)}</ul>
      </Col>
      <Jumbotron className="img-bg" fluid id="inicio">
        <Col className="layer"></Col>
        <Row>
          <Col className="d-block d-md-none navMovil">
            <Navbar bg="light" expand="lg">
              <Navbar.Brand href="#home" className="navbar-mobile">
                <img className="logo-mobile" src={logo} alt="medico en movil" />
                Médico Movil
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                {itemsNav(NAV, 'movil')}
              </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>

        <Row>
          <div className="p1-color row-online content">
            <div>
              <p className="h1 text-m1">
                <strong> Médico. En línea.</strong>
              </p>
            </div>
            <div>
              <img src={googlePlay} />
            </div>
            <div className='row-button'>
              <Button
                label="PRUEBA AHORA MISMO"
                handleOnChange={handleOnChange}
              />
            </div>
          </div>
        </Row>
      </Jumbotron>
    </>
  )
}

export default Navs;