import React from 'react';
import { Link } from "react-router-dom";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

const Consultation = () => (
  <Container fluid className="bg-section-consultation" id="consultas">
    <Row>
      <Col className="col-row-custom">
        <h3>Inicia una consulta ahora mismo</h3>
        <h6>No dudes más la consulta es gratuita y confidencial</h6>
        <br />
        <Link
          to="/payment">
          <Button
            variant="primary"
            className="btn-chat">
            Chat
          </Button>
        </Link>
      </Col>
      <Col>
        <img
          className="img-section-consultation"
          src="https://theme.visualmodo.com/medical/wp-content/uploads/sites/37/2017/07/medical-10.png" />
      </Col>
    </Row>
  </Container>
)

export default Consultation;