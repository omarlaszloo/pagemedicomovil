import React, { useEffect } from 'react';
import { InlineWidget } from 'react-calendly';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from 'react-bootstrap';
import logo from '../../assets/img/icon.png';
import Footer from '../../components/footer';


import './index.css';

const Schedule = () => {
  let { id } = useParams();
  const [showLoader, setShowLoader] = useState(true);
  const [expiredId, setExpiredId] = useState()

  const handlePaymentValidate = async () => {
    try {
      const resp = await fetch(`http://localhost:4242/payment/${id}/validate`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const { response } = await resp.json();
      if (response?.expired_reference_id) {
        setExpiredId(true)
      }
      setShowLoader(!response?.success_payment);
    } catch (errorPayment) {
      console.log(':: errorPayment', errorPayment)
    }
  }

  useEffect(() => {
    handlePaymentValidate(id);
  }, [])

  return (
    <>

      <Container fluid>
        <Row>
          <Col className="bg-left-login">
            <Col className="layer-login"></Col>
            <Col className="p1-color row-online-login">
              <p className="h2 label-title-left">
                <p>Tu pago fue exitoso</p>
                <p>Ahora solo tienes que agendar tu cita.</p>
              </p>
            </Col>
          </Col>
          <Col className="bg-col-right">
            <div className="col-right">
              <div className="success-payment-logo">
                <img
                  className="logo-mobile"
                  src={logo}
                  alt="medico en movil"
                />
                <p className="label-title">Médico. En linea</p>
              </div>
              <div>
                {!expiredId ? (
                  <div className='calendar'>
                    <div className="App">
                      <InlineWidget url="https://calendly.com/omarlaszloo" />
                    </div>
                  </div>
                ) : <div>Expirado la sesion para la llamada</div>}
              </div>

            </div>
          </Col>
        </Row>
      </Container >
      <Footer />


    </>

  )
}

export default Schedule;