import firebase from 'firebase/app'

const firebaseConfig = {
  apiKey: "AIzaSyCcrCFc5dRjZA3yv22XTcQ-y4yAPAAmCys",
  authDomain: "chat-doctor-d5556.firebaseapp.com",
  databaseURL: "https://chat-doctor-d5556.firebaseio.com",
  projectId: "chat-doctor-d5556",
  storageBucket: "chat-doctor-d5556.appspot.com",
  messagingSenderId: "185564122175",
  appId: "1:185564122175:web:dc6a82ac228869a192fc8a"
}

export const firebaseApp = firebase.initializeApp(firebaseConfig)
