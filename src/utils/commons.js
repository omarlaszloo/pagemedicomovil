import axios from 'axios';
import * as firebase from 'firebase';
import { reject } from 'lodash';


const uri = 'https://6c5e67cc1589.ngrok.io/'

export const distpatchEmail = path => {
  axios.get(uri + path)
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    })
}

export const distpatchContacts = (path, data) => {
  axios.get(uri + path, {
    params: {
      name: data.name,
      message: data.message,
      email: data.email
    }
  }, { headers: { "Access-Control-Allow-Origin": "*" } })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    })
}

export const createUser = (valueA, valueB) => {
  let p1 = new Promise((resolve, reject) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(valueA, valueB)
      .then(() => {
        return resolve(200);
      })

  })

  let response = p1.then((resp => {
    if (resp === 200) {
      return resp
    }

  })
  )
  return response;
}

// login
export const singIn = (email, password) => {
  let p1 = new Promise((resolve, reject) => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        return resolve(200)

      })
      .catch(() => {
        reject(500)
      })
  })

  let response = p1.then((resp) => {
    if (resp === 200) {
      return resp
    }
  })
    .catch((error) => {
      return error
    })

  return response
}


export const savePatient = (payload, dbFirestore) => {
  const db = firebase.firestore();
  let p1 = new Promise((resolve, reject) => {
    db.collection(dbFirestore.name).doc().set(payload)
      .then(() => {
        resolve(201)
      })
      .catch(() => {
        reject(500)
      })
  })

  let response = p1.then((resp => {
    if (resp === 201) {
      return resp
    }
  }))
    .catch((error) => {
      return error
    })

  return response
}

export const getDbFirestore = params => {
  console.log('params get ++', params)
  const db = firebase.firestore();
  let p1 = new Promise((resolve, reject) => {
    db.collection(params.name).get().then((querySnapshot) => {
      resolve(querySnapshot)
    })
      .catch(() => {
        reject(500)
      })
  })

  let response = p1.then((resp => {
    let items = []
    resp.forEach((doc) => {
      const p1 = JSON.stringify(doc.data())
      items.push(JSON.parse(p1))
    });

    return items
  }))
    .catch((error) => {
      return error
    })

  return response
}

export const realtimeChatWrite = (userName, message, time, nameDatabase) => {
  firebase
    .database()
    .ref(nameDatabase)
    .push({ userName, text: message, time })
}


export const currentUser = async () => {
  let p1 = await firebase.auth().currentUser
  return p1
}

export const connectDbChat = (nameDatabase, setMessage) => {
  const chat = firebase.database().ref(nameDatabase)
  chat.on("value", (snapshot) => {
    setMessage(snapshot.val())
  })
}

export const saveImage = (random) => {
  const save = firebase.storage().ref().child(`imgConsultas/${random}`)
  console.log('save imagen', save)
}

export const saveImagenDb = (random) => {

  let p1 = new Promise((resolve, reject) => {
    firebase
      .storage()
      .ref(`imgConsultas/${random}`)
      .getDownloadURL()
      .then((response) => {
        console.log(response)
        const res = {
          status: 200,
          response
        }
        /* const update = {
          photoURL: response
        }
        console.log('listo para mandar al chat la imagen', update)
        sendMessage(update) */
        resolve(res)

      })
      .catch(() => {
        console.log("Error al actualizar el avatar")
        reject(500)
      })
  })

  let response = p1.then((resp => {
    if (resp.res.status === 200) {
      return resp
    }
  }))
    .catch((error) => {
      return error
    })

  return response

}

export const defaultFormValue = () => {
  return {
    email: '',
    password: '',
    repeatpassword: ''
  }
}

export const defaultFormContacs = () => {
  return {
    name: '',
    email: '',
    message: ''
  }
}

export const defaultFormChats = () => {
  return {
    textChat: '',
  }
}


export const saveFormPatient = () => {
  return {
    weight: '',
    fever: '',
    height: '',
    allergy: '',
    last_consultation: '',
    observations: '',
    name: '',
    year: '',
    sexo: '',
    city: ''
  }
}


