
import {
  faUserMd,
  faCreditCard,
  faHandshake,
  faComments,
  faCircle,
  faUserAlt,
  faAddressCard,
  faNotesMedical,
  faChartPie
} from '@fortawesome/free-solid-svg-icons';

export const LIST_MENU = [
  {
    admin: [
      {
        label: "Activo",
        icon: faCircle
      },
      {
        label: "Metricas",
        icon: faChartPie
      },
      {
        label: "Médico",
        icon: faUserMd
      },
      {
        label: "Pagos",
        icon: faCreditCard
      },
      {
        label: "Comisiones",
        icon: faHandshake
      },
      {
        label: "Chat",
        icon: faComments
      }
    ]
  },
  {
    user: [
      {
        label: "Activo",
        icon: faCircle
      },
      {
        label: "Mi Perfil",
        icon: faUserAlt
      },
      {
        label: "Mis datos clinicos",
        icon: faAddressCard
      },
      {
        label: "Mis consultas",
        icon: faNotesMedical
      },
      {
        label: "Chat",
        icon: faComments
      }
    ]
  }
]