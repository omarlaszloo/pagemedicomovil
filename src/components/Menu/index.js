import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './index.css';

const Menu = ({
  actionItem = () => { },
  items = {},
}) => (
  <ul className="menu">
    {
      items.map(item => (
        <li>
          <a className="item-icon"
            onClick={() => actionItem(item?.label)}>
            <FontAwesomeIcon
              icon={item?.icon}
              size="1x"
              className="icon"
              color={item?.label === "Activo" ? "green" : ""}
            />
            {item.label}
          </a>
        </li>
      ))
    }
  </ul>
)

export default Menu