import React from 'react';
import { map } from 'lodash';
import "./index.css"

const Message = (props) => {

    const { message: { userName, text, time }, name } = props;
    const thisIsMe = userName === userName;
    const conditionalStyle = {
        container: thisIsMe && userName === "Paciente" ?  "container-1": "container-2",
        viewMessage: thisIsMe && userName === "Paciente" ? "viewMessage-1": "viewMessage-2",
    }

    const viewMsg = conditionalStyle.viewMessage
    const isImg = text.photoURL ? true : false;
    
    return (
        <div className="container">
            <div className={viewMsg}>
                <div>
                    {!isImg && text}
                </div>
                <div className="time">
                    {time}
                </div>

                {
                    isImg &&
                        map(text, (text, index) => {
                            console.log(text)
                            return(
                                <img
                                    className="container-img-chat"
                                    src={text}
                                />
                            )
                        })
                }

            </div>
        </div>

    )
}

export default Message