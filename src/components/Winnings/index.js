import React from 'react'
import Button from 'react-bootstrap/Button';

const Winnings = (props) => {
    const { showModalForm } = props
    return (
        <div className="container-winnings">
            <Button
                variant="primary"
                onClick={() => showModalForm()}
                className="btn-online">
                Configuración comisiones
            </Button>
        </div>
    )
}

export default Winnings