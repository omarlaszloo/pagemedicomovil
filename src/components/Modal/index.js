import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";

const Modals = ({
  textBody,
  textDescription,
  success,
  visibleClose,
  sizeModal = "lg",
  onHide,
  show,
}) => {

  return (
    <Modal
      show={show}
      size={sizeModal}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          <h4>{textBody}</h4>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {textDescription}

      </Modal.Body>
      <Modal.Footer>
        {success.visible &&
          <Link
            to={success.link}>
            <Button
              onClick={onHide}>
              Aceptar
            </Button>
          </Link>
        }{
          visibleClose &&
          <Button
            variant="danger"
            onClick={onHide}>
            Cerrar
          </Button>
        }
      </Modal.Footer>
    </Modal>
  )
}

export default Modals