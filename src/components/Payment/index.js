import React, { useState } from 'react'
import ToggleButton from 'react-bootstrap/ToggleButton'

const Payment = () => {
    const [checked, setChecked] = useState(false);

    const textOn = checked ? 'Prendido' : 'Apagado'
    const colorOn = checked ? 'primary' : 'secondary'

    return(
        <div className="container-payment">
            <p>Aqui podras apagar y prender el modulo de pagos</p>
            <div>
                <ToggleButton
                    type="checkbox"
                    variant={colorOn}
                    checked={checked}
                    value="1"
                    onChange={(e) => setChecked(e.currentTarget.checked)}>
                    {textOn}
                </ToggleButton>
            </div>
        </div>
    )
}

export default Payment