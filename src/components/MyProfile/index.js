import React from 'react'
import TlabeList from '../TableList'

const MyProfile = ({
    dataClinic,
    showModalForm,
    section
}) => {
    return(
        <TlabeList
            dataClinic={dataClinic}
            showModalForm={showModalForm}
            section={section}
        />
    )
}

export default MyProfile