import React from 'react';
import Button from 'react-bootstrap/Button';

const Submit = ({
  text,
  variant,
  size,
  className,
  oAction,
}) => (
  <Button
    variant={variant}
    size={size}
    className={className}
    onClick={() => oAction()}
  >
    {text}
  </Button>
)

export default Submit;