import React from 'react';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faUserTimes } from '@fortawesome/free-solid-svg-icons';

import './index.css';

const TableList = ({
  dataClinic,
  currentPath,
  showModalForm,
  section,
  nameButton = "",
  eventEdit = () => { },
  handleDelete = () => { },
}) => {

  const listData = dataClinic !== undefined ? dataClinic : []

  const showTextByMenu = section => {
    switch (section) {
      case 'Mis Datos Clinicos':
        return <div>
          <p>Aún no tienes datos clinicos...</p>
          <Button
            variant="outline-info"
            onClick={() => showModalForm()}
          >
            Agrega tus datos clinicos
          </Button>
        </div>
      case 'Mis consultas':
        return <p>Aun no tienes consultas...</p>

      case 'Mi Perfil':
        return <div>
          <p>Aún no tienes tu perfil completo...</p>
          <Button
            variant="outline-info"
            onClick={() => showModalForm()}>
            Agrega datos a tu perfil
          </Button>
        </div>
      default:
        break
    }
  }

  const notInfoUser = () => (
    currentPath === '/setting' ? <div className="not-info-person">
      <p>Aún no tienes medicos para atender </p>
      <Button
        onClick={() => showModalForm()}
        className="btn-add-info-clinic">
        Agregar médicos
      </Button>
    </div> : <div className="not-info-person">
      {showTextByMenu(section)}
    </div>
  )

  return (
    <React.Fragment>
      <button
        onClick={() => showModalForm()}
        className="btn-add-info-clinic">
        {nameButton}
      </button>
      {!!listData.length ? (
        <table className="table">
          <tr>
            <th>#</th>
            <th>Avatar</th>
            <th>Nombre</th>
            <th>Edad</th>
            <th>Email</th>
            <th>Sexo</th>
            <th>Ciudad</th>
            <th>Editar</th>
            <th>Borrar</th>
          </tr>
          {
            listData.map((item, index) => (
              <tr>
                <td>{index + 1}</td>
                <td><span className="dot">as</span></td>
                <td>{item?.name}</td>
                <td>{item?.year}</td>
                <td>{item?.email}</td>
                <td>{item?.sexo}</td>
                <td>{item?.city}</td>
                <td>
                  <FontAwesomeIcon
                    icon={faPencilAlt}
                    size="2x"
                    className="td-icon"
                    color={"#5499C7"}
                    onClick={() => eventEdit()}
                  />
                </td>
                <td>
                  <FontAwesomeIcon
                    icon={faUserTimes}
                    size="2x"
                    className="td-icon"
                    color={"#C0392B"}
                    onClick={() => handleDelete()}
                  />
                </td>
              </tr>
            ))
          }
        </table>
      ) : notInfoUser()
      }
    </React.Fragment >
  )
}

export default TableList