import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './index.css'

const Footer = () => (
  <Row className="bg-footer">
    <Col className="col-social">
      <p>Búscanos en ...</p>
      <ul className="footer">
        <li>
          <a href="">Youtube</a>
        </li>
        <li>
          <a href="">Facebook</a>
        </li>
        <li>
          <a href="">Whatsapp</a>
        </li>
        <li>
          <a href="">Gmail</a>
        </li>
      </ul>
      <br />
      <p className="terms-cand-conditions">Términos y condiciones</p>
      <p className="year">@2023</p>
    </Col>
  </Row>
)

export default Footer;