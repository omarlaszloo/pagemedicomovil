import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

const ModalForm = props => {
    
    const {
        actionSaveForm,
        onHide,
        children,
        title
    } = props

    return(
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    <h4>{title}</h4>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onHide}>cerrar</Button>
                <Button
                    variant="success"
                    onClick={() => actionSaveForm()}>
                    Aceptar
                </Button>
                
            </Modal.Footer>
        </Modal>
    )
}

export default ModalForm