import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faUserTimes } from '@fortawesome/free-solid-svg-icons';

const DataTable = ({
  listHeader = [],
  listData = [],
  handleEdit = () => { },
  handleDelete = () => { },
  configTable = {}
}) => {
  return (
    <table className="table">
      <tr>
        {configTable?.headers.map((item) => <th>{item?.title}</th>)}
      </tr>
      {
        listData.map((item, index) => (
          <tr>
            <td>{index + 1}</td>
            <td><span className="dot">as</span></td>
            <td>{item?.name}</td>
            <td>{item?.year}</td>
            <td>{item?.email}</td>
            <td>{item?.sexo}</td>
            <td>{item?.city}</td>
            {configTable?.isEditAvailable && (
              <td>
                <FontAwesomeIcon
                  icon={faPencilAlt}
                  size="2x"
                  className="td-icon"
                  color={"#5499C7"}
                  onClick={() => handleEdit()}
                />
              </td>
            )}
            {configTable?.isDeleteAvailable && (
              <td>
                <FontAwesomeIcon
                  icon={faUserTimes}
                  size="2x"
                  className="td-icon"
                  color={"#C0392B"}
                  onClick={() => handleDelete()}
                />
              </td>
            )}
          </tr>
        ))
      }
    </table>
  )
}

export default DataTable;
