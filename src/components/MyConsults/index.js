import React from 'react'
import TlabeList from '../TableList'


const MyConsults = ({ dataClinic, section }) => {
    return(
        <TlabeList
            dataClinic={dataClinic}
            section={section}
        />
    )
}

export default MyConsults