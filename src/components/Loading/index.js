import React from 'react'
import './css/styles.css'

const Loading = ({ showLoading = false, message = 'Cargando...' }) => (
  showLoading && (
    <div className="loading">
      <p className="loading-text">{message}</p>
    </div>
  )
)

export default Loading;
