import React from 'react'
import avatarUser from "../../assets/img/avatarUser.png"
import avatarDoctor from '../../assets/img/822712_user_512x512.png'
import "./index.css"

const ProfileHeader = ({ name, currentPath }) => {
  const path = currentPath === '/setting' ? avatarDoctor : avatarUser
  return (
    <React.Fragment>
      <div className="container-avatar">
        <div className="container-avatar">
          <img
            src={path}
            className="avatar"
            alt="Avatar"
          />
        </div>
        <div className="content-description">
          <p className="container-title">
            <strong>{`Hola ${name}`} </strong>
          </p>
        </div>
      </div>
    </React.Fragment>
  )
}

export default ProfileHeader
