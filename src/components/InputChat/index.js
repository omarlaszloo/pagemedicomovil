import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {
    currentUser,
    defaultFormChats,
    saveImage,
    saveImagenDb
} from '../../utils/commons';

import uuid from 'react-uuid'
import "./index.css"


const InputChat = (props) => {
    const {
        sendMessage,
        uid
    } = props;

    const [userInfo, setUserInfo] = useState(null);
    const [message, setMesssage] = useState(defaultFormChats());
    const [fileName, setFileName] = useState('');

    const onChange = (e, type) => {
        setMesssage({ ...message, [type]: e.target.value })
    }

    
    const onSubmit = () => {
        console.log('fileName', fileName)
        if (message.textChat.length > 0) {
            sendMessage(message)
            setMesssage(defaultFormChats())
        }

        const random = uuid();
        console.log('random', random)
        uploadImage(fileName, random).then(() => {
            updatePhotoUrl(random)
        })
        .catch(() => {
            console.log('Oooh ooh pasó algo inesperado')
        })
    }

    const onSubmitFile = () => {
        console.log('adjuntar archivo')
    }

    useEffect(() => {
        let p1 = currentUser()
        console.log('peticion 1', p1)
    }, []);

    const uploadImage = async (uri, random) => {
        console.log('uri', uri)
        const response = await fetch(uri)
        console.log('response', response)
        const blob = await response.blob()
        const ref = saveImage(random);
        return ref.put(response.url)
    }

    const updatePhotoUrl = random => {
        const save = saveImagenDb(random)
        console.log('save ======>>>>>>>', save)
    }

    return (
        <React.Fragment>
            <Form>
                <div className="container-form">
                    <div className="container-input">
                        <Form.Control
                            onChange={e => onChange(e, "textChat")}
                            type="text"
                            value={message.textChat}
                            placeholder="Escribe tu mensaje aqui!!"
                        />
                    </div>
                    <div className="container-btn">
                        <Button
                            onClick={() => onSubmit()}>
                            Enviar
                        </Button>
                    </div>
                </div>
                <div>
                    <Form.File
                        type="file"
                        onChange={e => setFileName(e.target.files[0].name)}
                        id="exampleFormControlFile1"
                        className="row-file"
                    />
                </div>
            </Form>
        </React.Fragment>
    )
}

export default InputChat