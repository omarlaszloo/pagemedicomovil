import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import avatar from "../../assets/img/PngItem_1985222.png";
import avatarSocial from "../../assets/img/avatar.png";
import Image from 'react-bootstrap/Image';


const MenuLeft = () => {
    return(
        <React.Fragment>
            <Row className="row-item-chat">
                <Col sm={3}>
                    <Image src={avatar} roundedCircle className="img-avatar-chat"/>
                </Col>
                <Col sm={8} className="item-text-chat">
                    <p><strong>Doc. Juan Pablo</strong> <br/>
                    <span>en linea</span>
                    </p>
                </Col>
            </Row>
            <Row className="row-item-chat">
                <Col sm={3}>
                    <Image src={avatarSocial} roundedCircle className="img-avatar-chat"/>
                </Col>
                <Col sm={8} className="item-text-chat">
                    <p><strong>Paciente</strong>
                    <br/>
                    <span>en linea</span>
                    </p>
                </Col>
            </Row>
        </React.Fragment>
    )
}

export default MenuLeft