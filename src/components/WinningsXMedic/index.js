import React from 'react'
import Button from 'react-bootstrap/Button';
import TlabeList from '../TableList'

const WinningsXMedic = (props) => {
    const { showModalForm, dataClinic, currentPath, section } = props
    return (
        
        <TlabeList
            dataClinic={dataClinic}
            currentPath={currentPath}
            showModalForm={showModalForm}
            section={section}
        />
        
    )
}

export default WinningsXMedic