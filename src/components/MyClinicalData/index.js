import React from 'react'
import TlabeList from '../TableList'
import './index.css'

const ClinicalData = ({
  dataClinic,
  currentPath,
  showModalForm,
  section
}) => {
  return (
    <TlabeList
      dataClinic={dataClinic}
      currentPath={currentPath}
      showModalForm={showModalForm}
      section={section}
    />
  )
}

export default ClinicalData