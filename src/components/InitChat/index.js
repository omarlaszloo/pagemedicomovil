import React from 'react'
import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import { distpatchEmail } from '../../utils/commons'
import './index.css'


const InitChat = ({ currentPath }) => {
    
    const send = () => {
        const params = 'send-email'
        return currentPath === '/setting' ? '' : distpatchEmail(params)
    }

    const uri = "https://c40aca52842d.ngrok.io/"
    
    return(
        <React.Fragment>
            <Link
                to="/chat">
                <Button
                    variant="primary"
                    onClick={() => send()}
                    className="btn-online">
                    INICIAR CHAT AHORA MISMO	
                </Button>
            </Link>

            {/* <div className="url-iframe">
                <iframe src={uri} width="800" height="600"></iframe>
        </div> */}
        </React.Fragment>
    )
}

export default InitChat