import React from 'react';
import Form from 'react-bootstrap/Form';
import Submit from '../../components/Button';

const Register = ({ registerUser, onChange }) => {
  return (
    <React.Fragment>
      <Form>
        <Form.Group controlId="formGroupName" className="space-group-m1">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Nombre"
          />
        </Form.Group>
        <Form.Group controlId="formGroupLastName" className="space-group-m1">
          <Form.Label>Apellidos</Form.Label>
          <Form.Control
            type="text"
            placeholder="Apellidos" />
        </Form.Group>

        <Form.Group controlId="formGroupEmail" className="space-group-m1">
          <Form.Label>Correo</Form.Label>
          <Form.Control
            type="email"
            placeholder="Correo"
            onChange={e => onChange(e, "email")}
          />
        </Form.Group>
        <Form.Group controlId="formGroupPassword" className="space-group-m1">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Contraseña"
            onChange={e => onChange(e, "password")}
          />
        </Form.Group>
        <Form.Group controlId="formGroupRepitPassword" className="space-group-m1">
          <Form.Label>Confirmar Contraseña</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirmar Contraseña"
          />
        </Form.Group>
        <Submit
          variant="primary"
          size="lg"
          className="btn-large space-group-m1"
          text="Enviar"
          oAction={() => registerUser()}
        />
      </Form>
    </React.Fragment>
  )
}

export default Register;