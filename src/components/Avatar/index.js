import React from "react"
import avatarDoctor from '../../assets/img/822712_user_512x512.png'
import "./index.css"

const Avatar = () => {
  return (
    <img
      src={avatarDoctor}
      alt="Avatar"
    />
  )
}

export default Avatar