import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const LoginComponent = ({ onChange, logIn }) => {
  return (
    <Form>
      <Form.Group controlId="formGroupEmail" className="space-group-m1">
        <Form.Label>Correo</Form.Label>
        <Form.Control
          type="email"
          placeholder="Correo"
          onChange={e => onChange(e, "email")}
        />
      </Form.Group>
      <Form.Group controlId="formGroupPassword" className="space-group-m1">
        <Form.Label>Contraseña</Form.Label>
        <Form.Control
          type="password"
          placeholder="Contraseña"
          onChange={e => onChange(e, "password")}
        />
      </Form.Group>

      <Button
        variant="primary"
        size="lg"
        block
        className="btn-large space-group-m1"
        onClick={logIn}>
        Iniciar sesión
      </Button>
    </Form>
  )
}

export default LoginComponent;
