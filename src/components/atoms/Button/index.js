import React from 'react';
import './index.css';

const Button = ({
  label,
  handleOnChange,
  extraStyles
}) => {
  return (
    <button
      className='button'
      onClick={handleOnChange}
      style={extraStyles}>
      {label}
    </button>
  )
}

Button.defaultProps = {
  label: null,
  handleOnChange: () => null,
  extraStyles: {}
}

export default Button;