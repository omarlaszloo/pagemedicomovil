import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';

import Home from './pages/Home';
import Login from './pages/Login';
import Chat from './pages/Chat';
import Profile from './pages/Profile';
import Admin from './pages/Admin';
import Payments from './pages/Payments';
import Schedule from './pages/Schedule';

import './App.css';

const App = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/payment" component={Payments} />
    <Route path="/login" component={Login} />
    <Route path="/profile" component={Profile} />
    <Route path="/admin" component={Login} />
    <Route path="/chat" component={Chat} />
    <Route path="/setting" component={Admin} />
    <Route path='/verification/:id/success=true' component={Schedule} />
  </Switch>
);

export default App;
